import ConfigFileReader.ConfigReader;
import JavaGenerator.Generator;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class MainRandGen {
    public static void main(String[] args) throws FileNotFoundException {
        ConfigReader read = new ConfigReader();
        read.readConfigfile();
        Generator generator = new Generator("class_declaration");
        generator.codeGenerator();
    }
}
