package ConfigFileReader;
import ProdCollection.Lists;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.security.PublicKey;
import java.util.Collection;
import java.util.Properties;

public class ConfigReader {
    public String temp;
    public int productionLines=8;
    int count = 0;
    File configFile = new File ("C:\\Users\\ADARSH\\Desktop\\Oole Project\\src\\config.properties");

    public void readConfigfile(){
        try {
            FileReader configread = new FileReader(configFile);
            Properties getKeyValue = new Properties();
            getKeyValue.load(configread);

            for (int i = 1; i <= productionLines; i++) {
                temp = getKeyValue.getProperty(i + "");

                String[] lineIdentity = temp.split("::=");
                String[] rhsOfLine = lineIdentity[1].split("\\|");
                int minval = count;
                for (String s : rhsOfLine) {
                    count++;
                    Lists.prodRHS.add(s);

                }

                int maxval = count - 1;
                lineIdentity[0] = lineIdentity[0].trim();
                Lists.prodLHS.put(lineIdentity[0], new Integer[]{minval, maxval});
            }
        }
        catch(FileNotFoundException ex){
            ex.printStackTrace();
        } catch(IOException ex){
            ex.printStackTrace();
        }
    }
}
