package JavaGenerator;

import ProdCollection.Lists;
import com.mifmif.common.regex.Generex;

import java.util.ArrayList;
import java.util.Vector;


public class Generator {
    NodeGen<String> t_rootNode;
    NodeGen<String> t_currentNode;
    Vector<NodeGen<String >> leafNodes = new Vector<>();


    public Generator(String p_root) {
        t_rootNode = new NodeGen<String>(p_root);
        t_currentNode=t_rootNode;
    }
    boolean isRegex(String p_input){
        if(p_input.contains("regex"))
            return true;
        else
            return false;
    }
    public void addChildNodes(NodeGen<String>p_Node,int p_recDepth){
        String key = p_Node.getData();
        int production_rule;
        String production=null;
        IntGenerator rand;
        Integer[] value;
        Boolean flag=false;
        if (Lists.prodLHS.get(key) != null) {
            value = Lists.prodLHS.get(key);
            rand = new IntGenerator();
            while (!flag){
                flag=true;
                production_rule = rand.random(value[0], value[1]);
                production = Lists.prodRHS.get(production_rule);
                if (p_recDepth >= 3) {
                    if (production.contains(key)) {
                        flag = false;
                    }
                }
            }
            //production = production.trim();
            //production = production.replaceAll("\\s", "");
            String nodes[] = production.split("\"");
            for (String str : nodes) {
                p_Node.addChild(str);
            }
        }
        else if(isRegex(key)){
            key=key.substring(5,key.length());
            Generex g = new Generex(key); //give the regex as parameter here
            String str = g.random();
            p_Node.addChild(str);

        }
        else
            return;

    }
    public void fn1(NodeGen<String>p_node,int p_recdepth)
    {
        for (NodeGen<String> stringNode : p_node.getChildren()) {
            addChildNodes(stringNode,p_recdepth);
            if(stringNode.getData().equals(p_node.getData()))
                fn1(stringNode,++p_recdepth);
            else
                fn1(stringNode,p_recdepth);
        }
    }
    //Tree Printing start
    public void fn2(NodeGen<String >p_node){
        for (NodeGen<String> stringNode:p_node.getChildren()){
            System.out.print(stringNode.getData()+"\t");
        }
        for (NodeGen<String> stringNode:p_node.getChildren()){
            fn2(stringNode);
        }
    }
    public void printTree()
    {
        t_currentNode=t_rootNode;
        System.out.println(t_currentNode.getData());
        for (NodeGen<String> stringNode:t_currentNode.getChildren()){
            System.out.print(stringNode.getData()+"\t");
        }
        System.out.println();
        for (NodeGen<String> stringNode:t_currentNode.getChildren()){
            fn2(stringNode);
        }

    }
    //Tree Printing end
    public void getAllLeafNodes(NodeGen<String>p_node) {

        if (p_node.getChildren().isEmpty()) {
            leafNodes.add(p_node);
        } else {
            for (NodeGen<String> child : p_node.getChildren()) {
                getAllLeafNodes(child);
            }
        }
    }
    public void codeGenerator() {

        String key = t_currentNode.getData();//didnt get the use of it
        addChildNodes(t_currentNode,0);
        fn1(t_currentNode,0);
        // printTree();
        //Set<Node<String>> finalCode=getAllLeafNodes(t_rootNode);
        getAllLeafNodes(t_rootNode);
        for (NodeGen<String>str:leafNodes) {
            System.out.print(str.getData());
        }
        System.out.println();

    }
}
