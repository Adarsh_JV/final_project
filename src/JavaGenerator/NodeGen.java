package JavaGenerator;

import java.util.ArrayList;
import java.util.List;

public class NodeGen<T>{
    private T data = null;
    private List<NodeGen> children = new ArrayList<>();
    private NodeGen parent = null;

    public NodeGen(T data) {
        this.data = data;
    }

    public void addChild(NodeGen child) {
        child.setParent(this);
        this.children.add(child);
    }

    public void addChild(T data) {
        NodeGen<T> newChild = new NodeGen<>(data);
        newChild.setParent(this);
        children.add(newChild);
    }
    public void addChildren(List<NodeGen> children) {
        for(NodeGen t : children) {
            t.setParent(this);
        }
        this.children.addAll(children);
    }

    public List<NodeGen> getChildren() {
        return children;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    private void setParent(NodeGen parent) {
        this.parent = parent;
    }

    public NodeGen getParent() {
        return parent;
    }
}